<?php
// Definiranje array-a
$intArray = array(1, 2, 3, 4, 5);
$intFloat = array(1.34, 1.43, 1.99, 1.52, 2.01);
$intText = array('Luka', 'Marko', 'Mario', 'Sven', 'Lovro');

// 1. array
unset($intArray[count($intArray) - 1]);
print_r($intArray);
echo '<br>';

// 2. array
array_push($intFloat, 5.23);
sort($intFloat);
print_r($intFloat);
echo '<br>';

// 3. array
rsort($intText);
print_r($intText);







?>

